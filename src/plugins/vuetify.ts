// Styles
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";

// Vuetify
import { createVuetify } from "vuetify";
import { VBtn } from "vuetify/lib/components/index.mjs";

export default createVuetify({
  aliases: {
    VBtnSave: VBtn,
    VBtnCancel: VBtn,
    VBtnTertiary: VBtn,
  },
  defaults: {
    VTextField: {
      variant: "underlined",
      density: "comfortable",
    },
    VTextarea: {
      variant: "underlined",
      density: "comfortable",
    },
    VSelect: {
      variant: "underlined",
      density: "comfortable",
    },
    VCheckbox: {
      density: "compact",
      hideDetails: true,
    },
    VBtnSave: {
      color: "success",
      size: "small",
    },
    VBtnCancel: {
      color: "error",
      size: "small",
    },
    VBtnTertiary: {
      size: "x-small",
      color: "grey-darken-3",
      variant: "tonal",
    },
  },
});
// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides

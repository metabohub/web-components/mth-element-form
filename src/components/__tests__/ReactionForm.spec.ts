import { describe, expect, test } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import ReactionForm from "@/components/ReactionForm.vue";
import { ReactionData } from "@/types/ReactionData";

const vuetify = createVuetify();
function reactionFormWrapper(data: ReactionData) {
  return mount(ReactionForm, {
    global: { plugins: [vuetify] },
    props: { data },
  });
}

async function testInvalidField(
  data: ReactionData,
  field: string,
  newContent: string
) {
  const wrapper = reactionFormWrapper(data);

  // there should be no error message and submit button must not be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(false);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeUndefined();

  // put the new content in the field
  wrapper.find(field).setValue(newContent);
  await flushPromises();

  // there should be an error message and submit button must be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(true);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeDefined();
}

describe("ReactionForm.vue", () => {
  test("should display a form with disabled submit button", async () => {
    const data = new ReactionData({});
    const wrapper = reactionFormWrapper(data);

    // expect inputs
    expect(wrapper.find("#id")).toBeTruthy();
    expect(wrapper.find("#name")).toBeTruthy();
    expect(wrapper.find("#ec")).toBeTruthy();
    expect(wrapper.find("#pathway")).toBeTruthy();
    expect(wrapper.find("#reversible")).toBeTruthy();
    expect(wrapper.find("#spontaneous")).toBeTruthy();
    expect(wrapper.find("#generic")).toBeTruthy();
    expect(wrapper.find("#hole")).toBeTruthy();
    expect(wrapper.find("#enzymComplex")).toBeTruthy();
    expect(wrapper.find("#confidence")).toBeTruthy();
    expect(wrapper.find("#goNumber")).toBeTruthy();
    expect(wrapper.find("#goName")).toBeTruthy();
    expect(wrapper.find("#upperBound")).toBeTruthy();
    expect(wrapper.find("#lowerBound")).toBeTruthy();

    // buttons
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    // expect a disabled submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    //const submitBtn = wrapper.get("#submitBtn");
    await flushPromises();
    //expect(submitBtn.attributes("disabled")).toBeDefined();
  });
  test("should emit 'submit' event", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    const wrapper = reactionFormWrapper(data);

    // Trigger click on submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#submitBtn");
    submitBtn.trigger("submit");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().submit).toBeTruthy();
    // data emitted should be the prop
    // @ts-ignore
    expect(wrapper.emitted().submit[0][0].data).toEqual(data);
  });
  test("should emit 'cancel' event", async () => {
    const data = new ReactionData({});
    const wrapper = reactionFormWrapper(data);

    // Trigger click on cancel button
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    const cancelBtn = wrapper.get("#cancelBtn");
    cancelBtn.trigger("click");
    await flushPromises();
    // expect an event 'cancel' to have emitted
    expect(wrapper.emitted().cancel).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new pathway' button", async () => {
    const data = new ReactionData({});
    const wrapper = reactionFormWrapper(data);

    // Trigger click on pathway button
    expect(wrapper.find("#pathwayBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#pathwayBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'newForm' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new enzymatic complex' button", async () => {
    const data = new ReactionData({});
    const wrapper = reactionFormWrapper(data);

    // Trigger click on enzymatic complex button
    expect(wrapper.find("#enzymaticComplexBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#enzymaticComplexBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'newForm' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("expect initial values to be displayed when editing", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    const wrapper = reactionFormWrapper(data);

    // check input values
    expect((wrapper.find("#id").element as HTMLInputElement).value).toBe(
      data["id"]
    );
    expect((wrapper.find("#name").element as HTMLInputElement).value).toBe(
      data["name"]
    );
    expect((wrapper.find("#ec").element as HTMLInputElement).value).toBe(
      data["ec"]
    );

    // multiple select #pathway
    let selected = wrapper.findAll("#pathway .v-select__selection-text");
    const pathways = selected.map((s) => {
      return s.element.textContent?.replace(",", "");
    });
    expect(pathways).toStrictEqual(data["pathway"]);

    expect(
      (wrapper.find("#reversible").element as HTMLInputElement).checked
    ).toBe(data["reversible"]);

    expect(
      (wrapper.find("#spontaneous").element as HTMLInputElement).checked
    ).toBe(data["spontaneous"]);

    expect((wrapper.find("#generic").element as HTMLInputElement).checked).toBe(
      data["generic"]
    );

    expect((wrapper.find("#hole").element as HTMLInputElement).checked).toBe(
      data["hole"]
    );

    // multiple select #enzymComplex
    selected = wrapper.findAll("#enzymComplex .v-select__selection-text");
    const enzymComp = selected.map((s) => {
      return s.element.textContent?.replace(",", "");
    });
    expect(enzymComp).toStrictEqual(data["enzymComp"]);

    expect(
      (wrapper.find("#confidence").element as HTMLInputElement).value
    ).toBe(data["confidence"]);

    expect((wrapper.find("#goNumber").element as HTMLInputElement).value).toBe(
      data["goNumber"]
    );

    expect((wrapper.find("#goName").element as HTMLInputElement).value).toBe(
      data["goName"]
    );

    expect(
      (wrapper.find("#upperBound").element as HTMLInputElement).value
    ).toBe(data["upperBound"]);

    expect(
      (wrapper.find("#lowerBound").element as HTMLInputElement).value
    ).toBe(data["lowerBound"]);
  });
  /*
  test("id field should display an error message when empty", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    await testInvalidField(data, "#id", "");
  });
  */
  test("id field should display an error message when invalid", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    await testInvalidField(data, "#id", "%%");
  });
  test("name field should display an error message when empty", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    await testInvalidField(data, "#name", "");
  });
  test("ec field should display an error message when empty", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    await testInvalidField(data, "#ec", "%%");
  });
  test("goNumber field should display an error message when empty", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    await testInvalidField(data, "#goNumber", "not a number");
  });
  test("upperBound field should display an error message when empty", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    await testInvalidField(data, "#upperBound", "not a number");
  });
  test("lowerBound field should display an error message when empty", async () => {
    const data: ReactionData = new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      //@ts-ignore
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    });
    await testInvalidField(data, "#lowerBound", "not a number");
  });
});

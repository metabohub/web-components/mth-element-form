import { describe, expect, test } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import GeneProductForm from "@/components/GeneProductForm.vue";
import { GeneProductData } from "@/types/GeneProductData";

const vuetify = createVuetify();
function geneProductFormWrapper(data: GeneProductData) {
  return mount(GeneProductForm, {
    global: { plugins: [vuetify] },
    props: { data },
  });
}

async function testInvalidField(
  data: GeneProductData,
  field: string,
  newContent: string
) {
  const wrapper = geneProductFormWrapper(data);

  // there should be no error message and submit button must not be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(false);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeUndefined();

  // put the new content in the field
  wrapper.find(field).setValue(newContent);
  await flushPromises();

  // there should be an error message and submit button must be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(true);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeDefined();
}

describe("GeneProductForm.vue", () => {
  test("should display a form with disabled submit button", async () => {
    const data = new GeneProductData({});
    const wrapper = geneProductFormWrapper(data);

    // expect inputs
    expect(wrapper.find("#id")).toBeTruthy();
    expect(wrapper.find("#name")).toBeTruthy();
    expect(wrapper.find("#compartment")).toBeTruthy();
    expect(wrapper.find("#gene")).toBeTruthy();
    expect(wrapper.find("#enzymComp")).toBeTruthy();

    // buttons
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    // expect a disabled submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    //const submitBtn = wrapper.get("#submitBtn");
    await flushPromises();
    //expect(submitBtn.attributes("disabled")).toBeDefined();
  });
  test("should emit 'submit' event", async () => {
    const data: GeneProductData = new GeneProductData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      enzymComp: ["EG50003-MONOMER"],
    });
    const wrapper = geneProductFormWrapper(data);

    // Trigger click on submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#submitBtn");
    submitBtn.trigger("submit");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().submit).toBeTruthy();
    // data emitted should be the prop
    // @ts-ignore
    expect(wrapper.emitted().submit[0][0].data).toEqual(data);
  });
  test("should emit 'cancel' event", async () => {
    const data = new GeneProductData({});
    const wrapper = geneProductFormWrapper(data);

    // Trigger click on cancel button
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    const cancelBtn = wrapper.get("#cancelBtn");
    cancelBtn.trigger("click");
    await flushPromises();
    // expect an event 'cancel' to have emitted
    expect(wrapper.emitted().cancel).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new compartment' button", async () => {
    const data = new GeneProductData({});
    const wrapper = geneProductFormWrapper(data);

    // Trigger click on compartment button
    expect(wrapper.find("#compartmentBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#compartmentBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'newForm' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new gene' button", async () => {
    const data = new GeneProductData({});
    const wrapper = geneProductFormWrapper(data);

    // Trigger click on gene button
    expect(wrapper.find("#geneBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#geneBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'newForm' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new enzymatic complex' button", async () => {
    const data = new GeneProductData({});
    const wrapper = geneProductFormWrapper(data);

    // Trigger click on enzymatic complex button
    expect(wrapper.find("#enzymaticComplexBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#enzymaticComplexBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'newForm' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("expect initial values to be displayed when editing", async () => {
    const data: GeneProductData = new GeneProductData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      enzymComp: ["EG50003-MONOMER"],
    });
    const wrapper = geneProductFormWrapper(data);

    // check input values
    expect((wrapper.find("#id").element as HTMLInputElement).value).toBe(
      data["id"]
    );

    expect((wrapper.find("#name").element as HTMLInputElement).value).toBe(
      data["name"]
    );

    expect(
      (wrapper.find("#compartment").element as HTMLInputElement).value
    ).toBe(data["compartment"]);

    // multiple select #gene
    let selected = wrapper.findAll("#gene .v-select__selection-text");
    const genes = selected.map((s) => {
      return s.element.textContent?.replace(",", "");
    });
    expect(genes).toStrictEqual(data["gene"]);

    // multiple select #enzymComp
    selected = wrapper.findAll("#enzymComp .v-select__selection-text");
    const enzymComp = selected.map((s) => {
      return s.element.textContent?.replace(",", "");
    });
    expect(enzymComp).toStrictEqual(data["enzymComp"]);
  });
  /*
  test("id field should display an error message when empty", async () => {
    const data: GeneProductData = new GeneProductData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      enzymComp: ["EG50003-MONOMER"],
    });
    await testInvalidField(data, "#id", "");
  });
  */
  test("id field should display an error message when invalid", async () => {
    const data: GeneProductData = new GeneProductData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      enzymComp: ["EG50003-MONOMER"],
    });
    await testInvalidField(data, "#id", "%%");
  });
  test("name field should display an error message when empty", async () => {
    const data: GeneProductData = new GeneProductData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      enzymComp: ["EG50003-MONOMER"],
    });
    await testInvalidField(data, "#name", "");
  });
});

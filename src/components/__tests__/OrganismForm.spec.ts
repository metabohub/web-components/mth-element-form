import { describe, expect, test } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import OrganismForm from "@/components/OrganismForm.vue";

const vuetify = createVuetify();
function organismFormWrapper() {
  return mount(OrganismForm, {
    global: { plugins: [vuetify] },
  });
}

async function testInvalidField(field: string, newContent: string) {
  const wrapper = organismFormWrapper();

  // there should be no error message and submit button must not be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(false);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeUndefined();

  // put the new content in the field
  wrapper.find(field).setValue("some name");
  await flushPromises();
  wrapper.find(field).setValue(newContent);
  await flushPromises();

  // there should be an error message and submit button must be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(true);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeDefined();
}

describe("OrganismForm.vue", () => {
  test("should display a form with disabled submit button", async () => {
    const wrapper = organismFormWrapper();

    // expect inputs
    expect(wrapper.find("#name")).toBeTruthy();

    // buttons
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    // expect a disabled submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    //const submitBtn = wrapper.get("#submitBtn");
    await flushPromises();
    //expect(submitBtn.attributes("disabled")).toBeDefined();
  });
  test("should emit 'submit' event", async () => {
    const wrapper = organismFormWrapper();

    // write content in required field
    wrapper.find("#name").setValue("organism name");
    await flushPromises();

    // Trigger click on submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#submitBtn");
    submitBtn.trigger("submit");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().submit).toBeTruthy();
  });
  test("should emit 'cancel' event", async () => {
    const wrapper = organismFormWrapper();

    // Trigger click on cancel button
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    const cancelBtn = wrapper.get("#cancelBtn");
    cancelBtn.trigger("click");
    await flushPromises();
    // expect an event 'cancel' to have emitted
    expect(wrapper.emitted().cancel).toBeTruthy();
  });
  test("name field should display an error message when empty", async () => {
    await testInvalidField("#name", "");
  });
});

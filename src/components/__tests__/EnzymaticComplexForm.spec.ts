import { describe, expect, test } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import EnzymaticComplexForm from "@/components/EnzymaticComplexForm.vue";
import { EnzymaticComplexData } from "@/types/EnzymaticComplexData";

const vuetify = createVuetify();
function enzymaticComplexFormWrapper(data: EnzymaticComplexData) {
  return mount(EnzymaticComplexForm, {
    global: { plugins: [vuetify] },
    props: { data },
  });
}

async function testInvalidField(
  data: EnzymaticComplexData,
  field: string,
  newContent: string
) {
  const wrapper = enzymaticComplexFormWrapper(data);

  // there should be no error message and submit button must not be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(false);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeUndefined();

  // put the new content in the field
  wrapper.find(field).setValue(newContent);
  await flushPromises();

  // there should be an error message and submit button must be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(true);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeDefined();
}

describe("EnzymaticComplexForm.vue", () => {
  test("should display a form with disabled submit button", async () => {
    const data = new EnzymaticComplexData({});
    const wrapper = enzymaticComplexFormWrapper(data);

    // expect inputs
    expect(wrapper.find("#id")).toBeTruthy();
    expect(wrapper.find("#name")).toBeTruthy();
    expect(wrapper.find("#reaction")).toBeTruthy();
    expect(wrapper.find("#gene")).toBeTruthy();
    expect(wrapper.find("#compartment")).toBeTruthy();

    // buttons
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    // expect a disabled submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    //const submitBtn = wrapper.get("#submitBtn");
    await flushPromises();
    //expect(submitBtn.attributes("disabled")).toBeDefined();
  });
  test("should emit 'submit' event", async () => {
    const data: EnzymaticComplexData = new EnzymaticComplexData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      reaction: ["RXN-17155"],
      //@ts-ignore
      geneP: ["apo-[acyl carrier protein]"],
    });
    const wrapper = enzymaticComplexFormWrapper(data);

    // Trigger click on submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#submitBtn");
    submitBtn.trigger("submit");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().submit).toBeTruthy();
    // data emitted should be the prop
    // @ts-ignore
    expect(wrapper.emitted().submit[0][0].data).toEqual(data);
  });
  test("should emit 'cancel' event", async () => {
    const data = new EnzymaticComplexData({});
    const wrapper = enzymaticComplexFormWrapper(data);

    // Trigger click on cancel button
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    const cancelBtn = wrapper.get("#cancelBtn");
    cancelBtn.trigger("click");
    await flushPromises();
    // expect an event 'cancel' to have emitted
    expect(wrapper.emitted().cancel).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new reaction' button", async () => {
    const data = new EnzymaticComplexData({});
    const wrapper = enzymaticComplexFormWrapper(data);

    // Trigger click on reaction button
    expect(wrapper.find("#reactionBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#reactionBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'newForm' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new gene product' button", async () => {
    const data = new EnzymaticComplexData({});
    const wrapper = enzymaticComplexFormWrapper(data);

    // Trigger click on gene product button
    expect(wrapper.find("#geneProductBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#geneProductBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new compartment' button", async () => {
    const data = new EnzymaticComplexData({});
    const wrapper = enzymaticComplexFormWrapper(data);

    // Trigger click on compartment button
    expect(wrapper.find("#compartmentBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#compartmentBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("expect initial values to be displayed when editing", async () => {
    const data: EnzymaticComplexData = new EnzymaticComplexData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      // @ts-ignore
      reaction: ["RXN-17155"],
      // @ts-ignore
      geneP: ["apo-[acyl carrier protein]"],
    });
    const wrapper = enzymaticComplexFormWrapper(data);

    // check input values
    expect((wrapper.find("#id").element as HTMLInputElement).value).toBe(
      data["id"]
    );

    expect((wrapper.find("#name").element as HTMLInputElement).value).toBe(
      data["name"]
    );

    // multiple select #reaction
    let selected = wrapper.findAll("#reaction .v-select__selection-text");
    const reactions = selected.map((s) => {
      return s.element.textContent?.replace(",", "");
    });
    expect(reactions).toStrictEqual(data["reaction"]);

    // multiple select #gene
    selected = wrapper.findAll("#gene .v-select__selection-text");
    const geneP = selected.map((s) => {
      return s.element.textContent?.replace(",", "");
    });
    expect(geneP).toStrictEqual(data["geneP"]);

    // single select compartment
    expect(
      (wrapper.find("#compartment").element as HTMLInputElement).value
    ).toBe(data["compartment"]);
  });
  /*
  test("id field should display an error message when empty", async () => {
    const data: EnzymaticComplexData = new EnzymaticComplexData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      reaction: ["RXN-17155"],
      //@ts-ignore
      geneP: ["apo-[acyl carrier protein]"],
    });
    await testInvalidField(data, "#id", "");
  });
  */
  test("id field should display an error message when invalid", async () => {
    const data: EnzymaticComplexData = new EnzymaticComplexData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      reaction: ["RXN-17155"],
      //@ts-ignore
      geneP: ["apo-[acyl carrier protein]"],
    });
    await testInvalidField(data, "#id", "%%");
  });
  test("name field should display an error message when empty", async () => {
    const data: EnzymaticComplexData = new EnzymaticComplexData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      //@ts-ignore
      reaction: ["RXN-17155"],
      //@ts-ignore
      geneP: ["apo-[acyl carrier protein]"],
    });
    await testInvalidField(data, "#name", "");
  });
});

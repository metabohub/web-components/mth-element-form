import { describe, expect, test } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import MthElementForm from "@/components/MthElementForm.vue";
import type { ElementType } from "@/types/ElementType";
import type { ElementData } from "@/types/ElementData";
import BiosourceForm from "@/components/BiosourceForm.vue";
import CompartmentForm from "@/components/CompartmentForm.vue";
import EnzymaticComplexForm from "@/components/EnzymaticComplexForm.vue";
import GeneForm from "@/components/GeneForm.vue";
import GeneProductForm from "@/components/GeneProductForm.vue";
import MetaboliteForm from "@/components/MetaboliteForm.vue";
import OrganismForm from "@/components/OrganismForm.vue";
import PathwayForm from "@/components/PathwayForm.vue";
import ReactionForm from "@/components/ReactionForm.vue";
import { BiosourceData } from "@/types/BiosourceData";
import { CompartmentData } from "@/types/CompartmentData";
import { EnzymaticComplexData } from "@/types/EnzymaticComplexData";
import { GeneData } from "@/types/GeneData";
import { GeneProductData } from "@/types/GeneProductData";
import { MetaboliteData } from "@/types/MetaboliteData";
import { PathwayData } from "@/types/PathwayData";
import { ReactionData } from "@/types/ReactionData";

const vuetify = createVuetify();
function mthElementFormWrapper(
  elementType: ElementType,
  elementData: ElementData | null
) {
  return mount(MthElementForm, {
    global: { plugins: [vuetify] },
    props: { elementType, elementData },
  });
}

describe("MthElementForm.vue", () => {
  test("should provide the right props", async () => {
    const elementType: ElementType = "BIOSOURCE";
    let elementData: ElementData = new BiosourceData({
      id: "4545",
      version: "aa",
    });
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // biosource
    let form: any = wrapper.findComponent(BiosourceForm);
    expect(form.props("data")).toEqual(elementData);
    await wrapper.setProps({ elementData: null });
    expect(form.props("data")).toEqual(new BiosourceData({}));

    // compartment
    elementData = new CompartmentData({ id: "45545", name: "sf" });
    await wrapper.setProps({
      elementType: "COMPARTMENT",
      elementData: elementData,
    });
    form = wrapper.findComponent(CompartmentForm);
    expect(form.props("data")).toEqual(elementData);
    await wrapper.setProps({ elementData: null });
    expect(form.props("data")).toEqual(new CompartmentData({}));

    // enzymatic complex
    elementData = new EnzymaticComplexData({ id: "45545", name: "sf" });
    await wrapper.setProps({
      elementType: "ENZYMATIC_COMPLEX",
      elementData: elementData,
    });
    form = wrapper.findComponent(EnzymaticComplexForm);
    expect(form.props("data")).toEqual(elementData);
    await wrapper.setProps({ elementData: null });
    expect(form.props("data")).toEqual(new EnzymaticComplexData({}));

    // gene
    elementData = new GeneData({ id: "45545", name: "sf" });
    await wrapper.setProps({ elementType: "GENE", elementData: elementData });
    form = wrapper.findComponent(GeneForm);
    expect(form.props("data")).toEqual(elementData);
    await wrapper.setProps({ elementData: null });
    expect(form.props("data")).toEqual(new GeneData({}));

    // gene product
    elementData = new GeneProductData({ id: "45545", name: "sf" });
    await wrapper.setProps({
      elementType: "GENE_PRODUCT",
      elementData: elementData,
    });
    form = wrapper.findComponent(GeneProductForm);
    expect(form.props("data")).toEqual(elementData);
    await wrapper.setProps({ elementData: null });
    expect(form.props("data")).toEqual(new GeneProductData({}));

    // metabolite
    elementData = new MetaboliteData({ id: "45545", name: "sf" });
    await wrapper.setProps({
      elementType: "METABOLITE",
      elementData: elementData,
    });
    form = wrapper.findComponent(MetaboliteForm);
    expect(form.props("data")).toEqual(elementData);
    await wrapper.setProps({ elementData: null });
    expect(form.props("data")).toEqual(new MetaboliteData({}));

    // pathway
    elementData = new PathwayData({ id: "45545", name: "sf" });
    await wrapper.setProps({
      elementType: "PATHWAY",
      elementData: elementData,
    });
    form = wrapper.findComponent(PathwayForm);
    expect(form.props("data")).toEqual(elementData);
    await wrapper.setProps({ elementData: null });
    expect(form.props("data")).toEqual(new PathwayData({}));

    // reaction
    elementData = new ReactionData({ id: "45545", name: "sf" });
    await wrapper.setProps({
      elementType: "REACTION",
      elementData: elementData,
    });
    form = wrapper.findComponent(ReactionForm);
    expect(form.props("data")).toEqual(elementData);
    await wrapper.setProps({ elementData: null });
    expect(form.props("data")).toEqual(new ReactionData({}));
  });
  test("should pass the events from BiosourceForm", async () => {
    const elementType: ElementType = "BIOSOURCE";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(BiosourceForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(BiosourceForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();

    // event 'newForm'
    await wrapper.getComponent(BiosourceForm).vm.$emit("newForm");
    await flushPromises();
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should pass the events from CompartmentForm", async () => {
    const elementType: ElementType = "COMPARTMENT";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(CompartmentForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(CompartmentForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();
  });
  test("should pass the events from EnzymaticComplexForm", async () => {
    const elementType: ElementType = "ENZYMATIC_COMPLEX";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(EnzymaticComplexForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(EnzymaticComplexForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();

    // event 'newForm'
    await wrapper.getComponent(EnzymaticComplexForm).vm.$emit("newForm");
    await flushPromises();
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should pass the events from GeneForm", async () => {
    const elementType: ElementType = "GENE";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(GeneForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(GeneForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();

    // event 'newForm'
    await wrapper.getComponent(GeneForm).vm.$emit("newForm");
    await flushPromises();
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should pass the events from GeneProductForm", async () => {
    const elementType: ElementType = "GENE_PRODUCT";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(GeneProductForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(GeneProductForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();

    // event 'newForm'
    await wrapper.getComponent(GeneProductForm).vm.$emit("newForm");
    await flushPromises();
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should pass the events from MetaboliteForm", async () => {
    const elementType: ElementType = "METABOLITE";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(MetaboliteForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(MetaboliteForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();

    // event 'newForm'
    await wrapper.getComponent(MetaboliteForm).vm.$emit("newForm");
    await flushPromises();
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should pass the events from OrganismForm", async () => {
    const elementType: ElementType = "ORGANISM";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(OrganismForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(OrganismForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();
  });
  test("should pass the events from PathwayForm", async () => {
    const elementType: ElementType = "PATHWAY";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(PathwayForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(PathwayForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();

    // event 'newForm'
    await wrapper.getComponent(PathwayForm).vm.$emit("newForm");
    await flushPromises();
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("should pass the events from ReactionForm", async () => {
    const elementType: ElementType = "REACTION";
    const elementData = null;
    const wrapper = mthElementFormWrapper(elementType, elementData);

    // event 'cancel'
    await wrapper.getComponent(ReactionForm).vm.$emit("cancel");
    await flushPromises();
    expect(wrapper.emitted().cancel).toBeTruthy();

    // event 'submit'
    await wrapper.getComponent(ReactionForm).vm.$emit("submit");
    await flushPromises();
    expect(wrapper.emitted().submit).toBeTruthy();

    // event 'newForm'
    await wrapper.getComponent(ReactionForm).vm.$emit("newForm");
    await flushPromises();
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
});

import { describe, expect, test } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import CompartmentForm from "@/components/CompartmentForm.vue";
import { CompartmentData } from "@/types/CompartmentData";

const vuetify = createVuetify();
function compartmentFormWrapper(data: CompartmentData) {
  return mount(CompartmentForm, {
    global: { plugins: [vuetify] },
    props: { data },
  });
}

async function testInvalidField(
  data: CompartmentData,
  field: string,
  newContent: string
) {
  const wrapper = compartmentFormWrapper(data);

  // there should be no error message and submit button must not be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(false);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeUndefined();

  // put the new content in the field
  wrapper.find(field).setValue(newContent);
  await flushPromises();

  // there should be an error message and submit button must be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(true);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeDefined();
}

describe("CompartmentForm.vue", () => {
  test("should display a form with disabled submit button", async () => {
    const data = new CompartmentData({});
    const wrapper = compartmentFormWrapper(data);

    // expect inputs
    expect(wrapper.find("#id")).toBeTruthy();
    expect(wrapper.find("#name")).toBeTruthy();
    expect(wrapper.find("#dim")).toBeTruthy();
    expect(wrapper.find("#size")).toBeTruthy();
    expect(wrapper.find("#unit")).toBeTruthy();
    expect(wrapper.find("#constant")).toBeTruthy();
    expect(wrapper.find("#default")).toBeTruthy();
    expect(wrapper.find("#upperComp")).toBeTruthy();

    // buttons
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    // expect a disabled submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    //const submitBtn = wrapper.get("#submitBtn");
    await flushPromises();
    //expect(submitBtn.attributes("disabled")).toBeDefined();
  });
  test("should emit 'submit' event", async () => {
    const data: CompartmentData = new CompartmentData({
      id: "CCO-CYTOSOL",
      name: "cytosolic",
    });
    const wrapper = compartmentFormWrapper(data);

    // Trigger click on submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#submitBtn");
    submitBtn.trigger("submit");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().submit).toBeTruthy();
    // data emitted should be the prop
    // @ts-ignore
    expect(wrapper.emitted().submit[0][0].data).toEqual(data);
  });
  test("should emit 'cancel' event", async () => {
    const data = new CompartmentData({});
    const wrapper = compartmentFormWrapper(data);

    // Trigger click on cancel button
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    const cancelBtn = wrapper.get("#cancelBtn");
    cancelBtn.trigger("click");
    await flushPromises();
    // expect an event 'cancel' to have emitted
    expect(wrapper.emitted().cancel).toBeTruthy();
  });
  test("expect initial values to be displayed when editing", async () => {
    const data: CompartmentData = new CompartmentData({
      id: "CCO-CYTOSOL",
      name: "cytosolic",
    });
    const wrapper = compartmentFormWrapper(data);

    // check input values
    expect((wrapper.find("#id").element as HTMLInputElement).value).toBe(
      data["id"]
    );

    expect((wrapper.find("#name").element as HTMLInputElement).value).toBe(
      data["name"]
    );

    expect((wrapper.find("#dim").element as HTMLInputElement).value).toBe(
      data["dimension"].toString()
    );

    expect((wrapper.find("#size").element as HTMLInputElement).value).toBe(
      data["size"].toString()
    );

    expect((wrapper.find("#unit").element as HTMLInputElement).value).toBe(
      data["unit"]
    );

    expect(
      (wrapper.find("#constant").element as HTMLInputElement).checked
    ).toBe(data["constantSize"]);

    expect((wrapper.find("#default").element as HTMLInputElement).checked).toBe(
      data["defaultCmptm"]
    );

    expect((wrapper.find("#upperComp").element as HTMLInputElement).value).toBe(
      data["upperComp"]
    );
  });
  /*
  test("id field should display an error message when empty", async () => {
    const data: CompartmentData = new CompartmentData({
      id: "CCO-CYTOSOL",
      name: "cytosolic",
    });
    await testInvalidField(data, "#id", "");
  });
  */
  test("id field should display an error message when invalid", async () => {
    const data: CompartmentData = new CompartmentData({
      id: "CCO-CYTOSOL",
      name: "cytosolic",
    });
    await testInvalidField(data, "#id", "%%");
  });
  test("name field should display an error message when empty", async () => {
    const data: CompartmentData = new CompartmentData({
      id: "CCO-CYTOSOL",
      name: "cytosolic",
    });
    await testInvalidField(data, "#name", "");
  });
  test("dimension field should display an error message when not numeric", async () => {
    const data: CompartmentData = new CompartmentData({
      id: "CCO-CYTOSOL",
      name: "cytosolic",
    });
    await testInvalidField(data, "#dim", "not a number");
  });
  test("size field should display an error message when not numeric", async () => {
    const data: CompartmentData = new CompartmentData({
      id: "CCO-CYTOSOL",
      name: "cytosolic",
    });
    await testInvalidField(data, "#size", "not a number");
  });
});

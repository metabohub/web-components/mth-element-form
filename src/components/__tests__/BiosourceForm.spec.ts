import { describe, expect, test } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import BiosourceForm from "@/components/BiosourceForm.vue";
import { BiosourceData } from "@/types/BiosourceData";

const vuetify = createVuetify();
function biosourceFormWrapper(data: BiosourceData) {
  return mount(BiosourceForm, {
    global: { plugins: [vuetify] },
    props: { data },
  });
}

async function testInvalidField(
  data: BiosourceData,
  field: string,
  newContent: string
) {
  const wrapper = biosourceFormWrapper(data);

  // there should be no error message and submit button must not be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(false);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeUndefined();

  // put the new content in the field
  wrapper.find(field).setValue(newContent);
  await flushPromises();

  // there should be an error message and submit button must be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(true);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeDefined();
}

describe("BiosourceForm.vue", () => {
  test("should display a form with disabled submit button", async () => {
    const data = new BiosourceData({});
    const wrapper = biosourceFormWrapper(data);

    // expect inputs
    expect(wrapper.find("#id")).toBeTruthy();
    expect(wrapper.find("#name")).toBeTruthy();
    expect(wrapper.find("#version")).toBeTruthy();
    expect(wrapper.find("#srcDB")).toBeTruthy();
    expect(wrapper.find("#url")).toBeTruthy();
    expect(wrapper.find("#organism")).toBeTruthy();
    expect(wrapper.find("#strain")).toBeTruthy();
    expect(wrapper.find("#tissue")).toBeTruthy();
    expect(wrapper.find("#cellType")).toBeTruthy();
    expect(wrapper.find("#comment")).toBeTruthy();

    // buttons
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    // expect a disabled submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    //const submitBtn = wrapper.get("#submitBtn");
    await flushPromises();
    //expect(submitBtn.attributes("disabled")).toBeDefined();
  });
  test("should emit 'submit' event", async () => {
    const data: BiosourceData = new BiosourceData({
      id: "2981",
      name: "Arabidopsis thaliana",
      version: "06/05/2015",
      srcDB: "KEGG Genes Database",
      organism: "Arabidopsis thaliana",
      strain: "Global Network",
    });
    const wrapper = biosourceFormWrapper(data);

    // Trigger click on submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#submitBtn");
    submitBtn.trigger("submit");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().submit).toBeTruthy();
    // data emitted should be the prop
    // @ts-ignore
    expect(wrapper.emitted().submit[0][0].data).toEqual(data);
  });
  test("should emit 'cancel' event", async () => {
    const data = new BiosourceData({});
    const wrapper = biosourceFormWrapper(data);

    // Trigger click on cancel button
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    const cancelBtn = wrapper.get("#cancelBtn");
    cancelBtn.trigger("click");
    await flushPromises();
    // expect an event 'cancel' to have emitted
    expect(wrapper.emitted().cancel).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new organism' button", async () => {
    const data = new BiosourceData({});
    const wrapper = biosourceFormWrapper(data);

    // Trigger click on organsim button
    expect(wrapper.find("#organismBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#organismBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("expect initial values to be displayed and updated when editing", async () => {
    const data: BiosourceData = new BiosourceData({
      id: "2981",
      name: "Arabidopsis thaliana",
      version: "06/05/2015",
      srcDB: "KEGG Genes Database",
      organism: "Arabidopsis thaliana",
      strain: "Global Network",
    });
    const wrapper = biosourceFormWrapper(data);

    // check input values
    expect((wrapper.find("#id").element as HTMLInputElement).value).toBe(
      data["id"]
    );

    expect((wrapper.find("#name").element as HTMLInputElement).value).toBe(
      data["name"]
    );

    expect((wrapper.find("#version").element as HTMLInputElement).value).toBe(
      data["version"].toString()
    );

    expect((wrapper.find("#srcDB").element as HTMLInputElement).value).toBe(
      data["srcDB"].toString()
    );

    expect((wrapper.find("#url").element as HTMLInputElement).value).toBe(
      data["url"]
    );

    expect((wrapper.find("#organism").element as HTMLInputElement).value).toBe(
      data["organism"]
    );

    expect((wrapper.find("#strain").element as HTMLInputElement).value).toBe(
      data["strain"]
    );

    expect((wrapper.find("#tissue").element as HTMLInputElement).value).toBe(
      data["tissue"]
    );

    expect((wrapper.find("#cellType").element as HTMLInputElement).value).toBe(
      data["cellType"]
    );

    expect((wrapper.find("#comment").element as HTMLInputElement).value).toBe(
      data["comment"]
    );
  });
  /*
  test("id field should display an error message when empty", async () => {
    const data: BiosourceData = new BiosourceData({
      id: "2981",
      name: "Arabidopsis thaliana",
      version: "06/05/2015",
      srcDB: "KEGG Genes Database",
      organism: "Arabidopsis thaliana",
      strain: "Global Network",
    });
    await testInvalidField(data, "#id", "");
  });
  */
  test("id field should display an error message when invalid", async () => {
    const data: BiosourceData = new BiosourceData({
      id: "2981",
      name: "Arabidopsis thaliana",
      version: "06/05/2015",
      srcDB: "KEGG Genes Database",
      organism: "Arabidopsis thaliana",
      strain: "Global Network",
    });
    await testInvalidField(data, "#id", "not a number");
  });
  test("name field should display an error message when empty", async () => {
    const data: BiosourceData = new BiosourceData({
      id: "2981",
      name: "Arabidopsis thaliana",
      version: "06/05/2015",
      srcDB: "KEGG Genes Database",
      organism: "Arabidopsis thaliana",
      strain: "Global Network",
    });
    await testInvalidField(data, "#name", "");
  });
  test("url field should display an error message when not an URL", async () => {
    const data: BiosourceData = new BiosourceData({
      id: "2981",
      name: "Arabidopsis thaliana",
      version: "06/05/2015",
      srcDB: "KEGG Genes Database",
      organism: "Arabidopsis thaliana",
      strain: "Global Network",
    });
    await testInvalidField(data, "#url", "not an URL");
  });
});

import { describe, expect, test } from "vitest";
import { flushPromises, mount } from "@vue/test-utils";
import { createVuetify } from "vuetify";
import MetaboliteForm from "@/components/MetaboliteForm.vue";
import { MetaboliteData } from "@/types/MetaboliteData";

const vuetify = createVuetify();
function metaboliteFormWrapper(data: MetaboliteData) {
  return mount(MetaboliteForm, {
    global: { plugins: [vuetify] },
    props: { data },
  });
}

async function testInvalidField(
  data: MetaboliteData,
  field: string,
  newContent: string
) {
  const wrapper = metaboliteFormWrapper(data);

  // there should be no error message and submit button must not be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(false);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeUndefined();

  // put the new content in the field
  wrapper.find(field).setValue(newContent);
  await flushPromises();

  // there should be an error message and submit button must be disabled
  expect(wrapper.find(".v-input--error").exists()).toBe(true);
  //expect(wrapper.get("#submitBtn").attributes("disabled")).toBeDefined();
}

describe("MetaboliteForm.vue", () => {
  test("should display a form with disabled submit button", async () => {
    const data = new MetaboliteData({});
    const wrapper = metaboliteFormWrapper(data);

    // expect inputs
    expect(wrapper.find("#id")).toBeTruthy();
    expect(wrapper.find("#name")).toBeTruthy();
    expect(wrapper.find("#chemicalFormula")).toBeTruthy();
    expect(wrapper.find("#charge")).toBeTruthy();
    expect(wrapper.find("#weight")).toBeTruthy();
    expect(wrapper.find("#compartment")).toBeTruthy();
    expect(wrapper.find("#generic")).toBeTruthy();
    expect(wrapper.find("#kegg")).toBeTruthy();
    expect(wrapper.find("#chebi")).toBeTruthy();
    expect(wrapper.find("#inchi")).toBeTruthy();
    expect(wrapper.find("#inchikey")).toBeTruthy();
    expect(wrapper.find("#smiles")).toBeTruthy();
    expect(wrapper.find("#constant")).toBeTruthy();
    expect(wrapper.find("#boundary")).toBeTruthy();
    expect(wrapper.find("#side")).toBeTruthy();

    // buttons
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    // expect a disabled submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    //const submitBtn = wrapper.get("#submitBtn");
    await flushPromises();
    //expect(submitBtn.attributes("disabled")).toBeDefined();
  });
  test("should emit 'submit' event", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    const wrapper = metaboliteFormWrapper(data);

    // Trigger click on submit button
    expect(wrapper.find("#submitBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#submitBtn");
    submitBtn.trigger("submit");
    await flushPromises();
    // expect an event 'submit' to have been emitted
    expect(wrapper.emitted().submit).toBeTruthy();
    // data emitted should be the prop
    // @ts-ignore
    expect(wrapper.emitted().submit[0][0].data).toEqual(data);
  });
  test("should emit 'cancel' event", async () => {
    const data = new MetaboliteData({});
    const wrapper = metaboliteFormWrapper(data);

    // Trigger click on cancel button
    expect(wrapper.find("#cancelBtn")).toBeTruthy();
    const cancelBtn = wrapper.get("#cancelBtn");
    cancelBtn.trigger("click");
    await flushPromises();
    // expect an event 'cancel' to have emitted
    expect(wrapper.emitted().cancel).toBeTruthy();
  });
  test("should emit 'newForm' event when click on 'new compartment' button", async () => {
    const data = new MetaboliteData({});
    const wrapper = metaboliteFormWrapper(data);

    // Trigger click on compartment button
    expect(wrapper.find("#compartmentBtn")).toBeTruthy();
    const submitBtn = wrapper.get("#compartmentBtn");
    submitBtn.trigger("click");
    await flushPromises();
    // expect an event 'newForm' to have been emitted
    expect(wrapper.emitted().newForm).toBeTruthy();
  });
  test("expect initial values to be displayed when editing", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    const wrapper = metaboliteFormWrapper(data);

    // check input values
    expect((wrapper.find("#id").element as HTMLInputElement).value).toBe(
      data["id"]
    );

    expect((wrapper.find("#name").element as HTMLInputElement).value).toBe(
      data["name"]
    );

    expect(
      (wrapper.find("#chemicalFormula").element as HTMLInputElement).value
    ).toBe(data["chemicalFormula"]);

    expect((wrapper.find("#charge").element as HTMLInputElement).value).toBe(
      data["charge"]
    );

    expect((wrapper.find("#weight").element as HTMLInputElement).value).toBe(
      data["weight"]
    );

    expect(
      (wrapper.find("#compartment").element as HTMLInputElement).value
    ).toBe(data["compartment"]);

    expect((wrapper.find("#generic").element as HTMLInputElement).checked).toBe(
      data["generic"]
    );

    expect((wrapper.find("#kegg").element as HTMLInputElement).value).toBe(
      data["kegg"]
    );

    expect((wrapper.find("#chebi").element as HTMLInputElement).value).toBe(
      data["chebi"]
    );

    expect((wrapper.find("#inchi").element as HTMLInputElement).value).toBe(
      data["inchi"]
    );

    expect((wrapper.find("#inchikey").element as HTMLInputElement).value).toBe(
      data["inchikey"]
    );

    expect((wrapper.find("#smiles").element as HTMLInputElement).value).toBe(
      data["smiles"]
    );

    expect(
      (wrapper.find("#constant").element as HTMLInputElement).checked
    ).toBe(data["constant"]);

    expect(
      (wrapper.find("#boundary").element as HTMLInputElement).checked
    ).toBe(data["boundary"]);

    expect((wrapper.find("#side").element as HTMLInputElement).checked).toBe(
      data["side"]
    );
  });
  /*
  test("id field should display an error message when empty", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#id", "");
  });
  */
  test("id field should display an error message when invalid", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#id", "%%");
  });
  test("name field should display an error message when empty", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#name", "");
  });
  test("chemicalFormula field should display an error message when invalid", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#chemicalFormula", "%%");
  });
  test("charge field should display an error message when invalid", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#charge", "not a number");
  });
  test("weight field should display an error message when invalid", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#weight", "not a number");
  });
  test("kegg field should display an error message when invalid", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#kegg", "%%");
  });
  test("chebi field should display an error message when invalid", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#chebi", "%%");
  });
  test("inchikey field should display an error message when invalid", async () => {
    const data: MetaboliteData = new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      chemicalFormula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    });
    await testInvalidField(data, "#inchikey", "%%");
  });
});

import { GeneProductData } from "@/types/GeneProductData";
import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import GeneProductForm from "../components/GeneProductForm.vue";

const meta: Meta<typeof GeneProductForm> = {
  title: "GeneProductForm",
  component: GeneProductForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof GeneProductForm>;

export const Add: Story = {
  render: (args) => ({
    components: { GeneProductForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<GeneProductForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new GeneProductData({}),
  },
};
export const Edit: Story = {
  render: (args) => ({
    components: { GeneProductForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<GeneProductForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new GeneProductData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      enzymComp: ["EG50003-MONOMER"],
    }),
  },
};

import { EnzymaticComplexData } from "@/types/EnzymaticComplexData";
import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import EnzymaticComplexForm from "../components/EnzymaticComplexForm.vue";

const meta: Meta<typeof EnzymaticComplexForm> = {
  title: "EnzymaticComplexForm",
  component: EnzymaticComplexForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof EnzymaticComplexForm>;

export const Add: Story = {
  render: (args) => ({
    components: { EnzymaticComplexForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<EnzymaticComplexForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new EnzymaticComplexData({}),
  },
};
export const Edit: Story = {
  render: (args) => ({
    components: { EnzymaticComplexForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<EnzymaticComplexForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new EnzymaticComplexData({
      id: "EG50003-MONOMER",
      name: "apo-[acyl carrier protein]",
      reaction: ["RXN-17155"],
      geneP: ["apo-[acyl carrier protein]"],
    }),
  },
};

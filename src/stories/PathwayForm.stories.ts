import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import PathwayForm from "../components/PathwayForm.vue";
import { PathwayData } from "../types/PathwayData";

const meta: Meta<typeof PathwayForm> = {
  title: "PathwayForm",
  component: PathwayForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof PathwayForm>;

export const Add: Story = {
  render: (args) => ({
    components: { PathwayForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<PathwayForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new PathwayData({}),
  },
};
export const Edit: Story = {
  render: (args) => ({
    components: { PathwayForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<PathwayForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new PathwayData({
      id: "VALDEG-PWY",
      name: "L-valine degradation I",
      reaction: [
        "RXN-11213",
        "BRANCHED-CHAINAMINOTRANSFERVAL-RXN",
        "MEPROPCOA-FAD-RXN",
      ],
    }),
  },
};

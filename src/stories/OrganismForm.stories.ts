import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import OrganismForm from "../components/OrganismForm.vue";

const meta: Meta<typeof OrganismForm> = {
  title: "OrganismForm",
  component: OrganismForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof OrganismForm>;

export const Add: Story = {
  render: () => ({
    components: { OrganismForm },
    setup() {
      return {
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template: "<OrganismForm @submit='submit' @cancel='cancel' />",
  }),
};

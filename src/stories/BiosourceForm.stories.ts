import { BiosourceData } from "../types/BiosourceData";
import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import BiosourceForm from "../components/BiosourceForm.vue";

const meta: Meta<typeof BiosourceForm> = {
  title: "BiosourceForm",
  component: BiosourceForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof BiosourceForm>;

export const Add: Story = {
  render: (args) => ({
    components: { BiosourceForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<BiosourceForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new BiosourceData({}),
  },
};
export const Edit: Story = {
  render: (args) => ({
    components: { BiosourceForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<BiosourceForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new BiosourceData({
      id: "2981",
      name: "Arabidopsis thaliana",
      version: "06/05/2015",
      srcDB: "KEGG Genes Database",
      organism: "Arabidopsis thaliana",
      strain: "Global Network",
    }),
  },
};

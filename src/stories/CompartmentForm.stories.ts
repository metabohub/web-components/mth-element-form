import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import CompartmentForm from "../components/CompartmentForm.vue";
import { CompartmentData } from "../types/CompartmentData";

const meta: Meta<typeof CompartmentForm> = {
  title: "CompartmentForm",
  component: CompartmentForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof CompartmentForm>;

export const Add: Story = {
  render: (args) => ({
    components: { CompartmentForm },
    setup() {
      return {
        ...args,
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<CompartmentForm :data='data' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new CompartmentData({}),
  },
};
export const Edit: Story = {
  render: (args) => ({
    components: { CompartmentForm },
    setup() {
      return {
        ...args,
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<CompartmentForm :data='data' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new CompartmentData({
      id: "CCO-CYTOSOL",
      name: "cytosolic",
    }),
  },
};

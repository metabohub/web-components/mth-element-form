import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import GeneForm from "../components/GeneForm.vue";
import { GeneData } from "../types/GeneData";

const meta: Meta<typeof GeneForm> = {
  title: "GeneForm",
  component: GeneForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof GeneForm>;

export const Add: Story = {
  render: (args) => ({
    components: { GeneForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<GeneForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new GeneData({}),
  },
};
export const Edit: Story = {
  render: (args) => ({
    components: { GeneForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<GeneForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new GeneData({
      id: "G198A-50201",
      name: "HanXRQCPg0580151",
      geneProduct: ["HanXRQCPg0580151"],
    }),
  },
};

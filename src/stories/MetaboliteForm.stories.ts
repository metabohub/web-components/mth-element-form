import { MetaboliteData } from "./../types/MetaboliteData";
import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import MetaboliteForm from "../components/MetaboliteForm.vue";

const meta: Meta<typeof MetaboliteForm> = {
  title: "MetaboliteForm",
  component: MetaboliteForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof MetaboliteForm>;

export const Add: Story = {
  render: (args) => ({
    components: { MetaboliteForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<MetaboliteForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new MetaboliteData({}),
  },
};
export const Edit: Story = {
  render: (args) => ({
    components: { MetaboliteForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<MetaboliteForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new MetaboliteData({
      id: "CPD-8065",
      name: "verbascose",
      formula: "C30H52O26",
      charge: "0",
      weight: "828.27468",
      compartment: "cytosolic",
    }),
  },
};

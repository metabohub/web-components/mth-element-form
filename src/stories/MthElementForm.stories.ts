import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import MthElementForm from "../components/MthElementForm.vue";
import { ReactionData } from "@/types/ReactionData";

const meta: Meta<typeof MthElementForm> = {
  title: "MthElementForm",
  component: MthElementForm,
  tags: ["autodocs"],
  argTypes: {
    elementType: {
      control: "select",
      options: [
        "REACTION",
        "PATHWAY",
        "METABOLITE",
        "BIOSOURCE",
        "COMPARTMENT",
        "ENZYMATIC_COMPLEX",
        "GENE",
        "GENE_PRODUCT",
        "ORGANISM",
      ],
    },
  },
};

export default meta;
type Story = StoryObj<typeof MthElementForm>;

export const Default: Story = {
  render: (args) => ({
    components: { MthElementForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<MthElementForm :elementType='elementType' :elementData='elementData' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    elementType: "REACTION",
    elementData: new ReactionData({}),
  },
};

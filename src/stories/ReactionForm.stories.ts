import type { Meta, StoryObj } from "@storybook/vue3";
import { action } from "@storybook/addon-actions";
import ReactionForm from "../components/ReactionForm.vue";
import { ReactionData } from "../types/ReactionData";

const meta: Meta<typeof ReactionForm> = {
  title: "ReactionForm",
  component: ReactionForm,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof ReactionForm>;

export const Add: Story = {
  render: (args) => ({
    components: { ReactionForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<ReactionForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new ReactionData({}),
  },
};
export const Edit: Story = {
  render: (args) => ({
    components: { ReactionForm },
    setup() {
      return {
        ...args,
        newForm: action("newForm"),
        submit: action("submit"),
        cancel: action("cancel"),
      };
    },
    template:
      "<ReactionForm :data='data' @newForm='newForm' @submit='submit' @cancel='cancel' />",
  }),
  args: {
    data: new ReactionData({
      id: "RXN-14272",
      name: "(E)-hexadec-2-enoyl-CoA hydratase",
      ec: "4.2.1.17",
      reversible: true,
      enzymComp: ["G198A-57738-MONOMER"],
      upperBound: "99999",
      lowerBound: "-99999",
    }),
  },
};

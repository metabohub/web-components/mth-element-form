export class BiosourceData {
  id: string;
  name: string;
  version: string;
  srcDB: string;
  url: string;
  organism: string;
  strain: string;
  tissue: string;
  cellType: string;
  comment: string;
  constructor({
    id = "",
    name = "",
    version = "",
    srcDB = "",
    url = "",
    organism = "",
    strain = "",
    tissue = "",
    cellType = "",
    comment = "",
  }) {
    this.id = id;
    this.name = name;
    this.version = version;
    this.srcDB = srcDB;
    this.url = url;
    this.organism = organism;
    this.strain = strain;
    this.tissue = tissue;
    this.cellType = cellType;
    this.comment = comment;
  }
}

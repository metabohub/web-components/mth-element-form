export class GeneProductData {
  id: string;
  name: string;
  compartment: string;
  gene: Array<string>;
  enzymComp: Array<string>;
  constructor({
    id = "",
    name = "",
    compartment = "",
    gene = [],
    enzymComp = [],
  }) {
    this.id = id;
    this.name = name;
    this.compartment = compartment;
    this.gene = gene;
    this.enzymComp = enzymComp;
  }
}

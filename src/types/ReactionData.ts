export class ReactionData {
  id: string;
  name: string;
  ec: string;
  pathway: Array<string>;
  reversible: boolean;
  spontaneous: boolean;
  generic: boolean;
  hole: boolean;
  enzymComp: Array<string>;
  confidence: string;
  goNumber: string;
  goName: string;
  upperBound: string;
  lowerBound: string;
  constructor({
    id = "",
    name = "",
    ec = "",
    pathway = [],
    reversible = false,
    spontaneous = false,
    generic = false,
    hole = false,
    enzymComp = [],
    confidence = "",
    goNumber = "",
    goName = "",
    upperBound = "",
    lowerBound = "",
  }) {
    this.id = id;
    this.name = name;
    this.ec = ec;
    this.pathway = pathway;
    this.reversible = reversible;
    this.spontaneous = spontaneous;
    this.generic = generic;
    this.hole = hole;
    this.enzymComp = enzymComp;
    this.confidence = confidence;
    this.goNumber = goNumber;
    this.goName = goName;
    this.upperBound = upperBound;
    this.lowerBound = lowerBound;
  }
}

export class MetaboliteData {
  id: string;
  name: string;
  chemicalFormula: string;
  charge: string;
  weight: string;
  compartment: string;
  generic: boolean;
  kegg: string;
  chebi: string;
  inchi: string;
  inchikey: string;
  smiles: string;
  constant: boolean;
  boundary: boolean;
  side: boolean;
  constructor({
    id = "",
    name = "",
    chemicalFormula = "",
    charge = "",
    weight = "",
    compartment = "",
    generic = false,
    kegg = "",
    chebi = "",
    inchi = "",
    inchikey = "",
    smiles = "",
    constant = false,
    boundary = false,
    side = false,
  }) {
    this.id = id;
    this.name = name;
    this.chemicalFormula = chemicalFormula;
    this.charge = charge;
    this.weight = weight;
    this.compartment = compartment;
    this.generic = generic;
    this.kegg = kegg;
    this.chebi = chebi;
    this.inchi = inchi;
    this.inchikey = inchikey;
    this.smiles = smiles;
    this.constant = constant;
    this.boundary = boundary;
    this.side = side;
  }
}

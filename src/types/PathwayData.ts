export class PathwayData {
  id: string;
  name: string;
  reaction: Array<string>;
  constructor({ id = "", name = "", reaction = [] }) {
    this.id = id;
    this.name = name;
    this.reaction = reaction;
  }
}

export type ElementType =
  | "BIOSOURCE"
  | "ORGANISM"
  | "METABOLITE"
  | "PATHWAY"
  | "ENZYMATIC_COMPLEX"
  | "GENE_PRODUCT"
  | "GENE"
  | "REACTION"
  | "COMPARTMENT";

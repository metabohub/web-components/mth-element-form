export class CompartmentData {
  id: string;
  name: string;
  dimension: number;
  size: number;
  unit: string;
  constantSize: boolean;
  defaultCmptm: boolean;
  upperComp: string;
  constructor({
    id = "",
    name = "",
    dimension = 3,
    size = 1,
    unit = "",
    constantSize = true,
    defaultCmptm = false,
    upperComp = "",
  }) {
    this.id = id;
    this.name = name;
    this.dimension = dimension;
    this.size = size;
    this.unit = unit;
    this.constantSize = constantSize;
    this.defaultCmptm = defaultCmptm;
    this.upperComp = upperComp;
  }
}

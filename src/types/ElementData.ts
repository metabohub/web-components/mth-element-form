import type { BiosourceData } from "@/types/BiosourceData";
import type { OrganismData } from "@/types/OrganismData";
import type { MetaboliteData } from "@/types/MetaboliteData";
import type { PathwayData } from "@/types/PathwayData";
import type { EnzymaticComplexData } from "@/types/EnzymaticComplexData";
import type { GeneProductData } from "@/types/GeneProductData";
import type { GeneData } from "@/types/GeneData";
import type { ReactionData } from "@/types/ReactionData";
import type { CompartmentData } from "@/types/CompartmentData";

export type ElementData =
  | BiosourceData
  | OrganismData
  | MetaboliteData
  | PathwayData
  | EnzymaticComplexData
  | GeneProductData
  | GeneData
  | ReactionData
  | CompartmentData;

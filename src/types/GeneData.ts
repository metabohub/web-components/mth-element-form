export class GeneData {
  id: string;
  name: string;
  geneProduct: Array<string>;
  constructor({ id = "", name = "", geneProduct = [] }) {
    this.id = id;
    this.name = name;
    this.geneProduct = geneProduct;
  }
}

export class EnzymaticComplexData {
  id: string;
  name: string;
  reaction: Array<string>;
  geneP: Array<string>;
  compartment: string;
  constructor({
    id = "",
    name = "",
    reaction = [],
    geneP = [],
    compartment = "",
  }) {
    this.id = id;
    this.name = name;
    this.reaction = reaction;
    this.geneP = geneP;
    this.compartment = compartment;
  }
}

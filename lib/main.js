import MthElementForm from "@/components/MthElementForm.vue";
import { BiosourceData } from "@/types/BiosourceData";
import { CompartmentData } from "@/types/CompartmentData";
import { EnzymaticComplexData } from "@/types/EnzymaticComplexData";
import { GeneData } from "@/types/GeneData";
import { GeneProductData } from "@/types/GeneProductData";
import { MetaboliteData } from "@/types/MetaboliteData";
import { OrganismData } from "@/types/OrganismData";
import { PathwayData } from "@/types/PathwayData";
import { ReactionData } from "@/types/ReactionData";

export {
  MthElementForm,
  BiosourceData,
  CompartmentData,
  EnzymaticComplexData,
  GeneData,
  GeneProductData,
  MetaboliteData,
  OrganismData,
  PathwayData,
  ReactionData,
};

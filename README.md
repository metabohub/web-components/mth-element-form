# mth-element-form (BETA VERSION)

Forms to add or modify elements of type `ElementData`.

## Types

| Name | Value |
| --- | --- |
| `ElementData` | `BiosourceData \| OrganismData \| MetaboliteData \| PathwayData \| EnzymaticComplexData \| GeneProductData \| GeneData \| ReactionData \| CompartmentData` |
| `ElementType` | `"BIOSOURCE" \| "ORGANISM" \| "METABOLITE" \| "PATHWAY" \| "ENZYMATIC_COMPLEX" \| "GENE_PRODUCT" \| "GENE" \| "REACTION" \| "COMPARTMENT"` |

## Classes

| Name | Parameters | Default |
| --- | --- | --- |
| `BiosourceData` | `id: string, name: string, version: string, srcDB: string, url: string, organism: string, strain: string, tissue: string, cellType: string, comment: string` | `id = "", name = "", version = "",srcDB = "", url = "", organism = "", strain = "", tissue = "", cellType = "", comment = ""` |
| `CompartmentData` | `id: string, name: string, dimension: number, size: number, unit: string, constantSize: boolean, defaultCmptm: boolean, upperComp: string` | `id = "", name = "", dimension = 3, size = 1, unit = "", constantSize = true, defaultCmptm = false, upperComp = ""` |
| `EnzymaticComplexData` | `id: string, name: string, reaction: Array<string>, geneP: Array<string>, compartment: string` | `id = "", name = "", reaction = [], geneP = [], compartment = ""` |
| `GeneData` | `id: string, name: string, geneProduct: Array<string>` | `id = "", name = "", geneProduct = []` |
| `GeneProductData` | `id: string, name: string, compartment: string, gene: Array<string>, enzymComp: Array<string>` | `id = "", name = "", compartment = "", gene = [], enzymComp = []` |
| `MetaboliteData` | `id: string, name: string, formula: string, charge: string, weight: string, compartment: string, generic: boolean, kegg: string, chebi: string, inchi: string, inchikey: string, smiles: string, constant: boolean, boundary: boolean, side: boolean` | `id = "", name = "", formula = "", charge = "", weight = "", compartment = "", generic = false, kegg = "", chebi = "", inchi = "", inchikey = "", smiles = "", constant = false, boundary = false, side = false` |
| `OrganismData` | `name: string` | `name = ""` |
| `PathwayData` | `id: string, name: string, reaction: Array<string>` | `id = "", name = "", reaction = []` |
| `ReactionData` | `id: string, name: string, ec: string, pathway: Array<string>, reversible: boolean, spontaneous: boolean, generic: boolean, hole: boolean, enzymComp: Array<string>, confidence: string, goNumber: string, goName: string, upperBound: string, lowerBound: string` | `id = "", name = "", ec = "", pathway = [], reversible = false, spontaneous = false, generic = false, hole = false, enzymComp = [], confidence = "", goNumber = "", goName = "", upperBound = "", lowerBound = ""` |

## Attributes

| Name          | Description                                                | Type                   | Default |
| ------------- | ---------------------------------------------------------- | ---------------------- | ------- |
| `elementType` | name of the element                                        | `ElementType`          | —       |
| `elementData` | data of the element to edit or `null` to add a new element | `ElementData  \| null` | —       |

## Events

| Name      | Description                                          | Parameters          |
| --------- | ---------------------------------------------------- | ------------------- |
| `newForm` | triggered when a button to add an element is clicked | `() => ElementType` |
| `submit`  | triggered when the form is submitted                 | `() => ElementData` |
| `cancel`  | triggered when click on the 'cancel' button          | `() => void`        |

## Use example

```
<MthElementForm
    :elementType="elementType"
    :elementData="elementData"
    @newForm="newForm($event)"
    @submit="submit($event)"
    @cancel="cancel"
  ></MthElementForm>
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

Check the coverage of the unit tests :
```sh
npm run coverage
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

### View documentation

```sh
npm run storybook
```

## CICD pipeline

### Tests

```
test:
  image: node:latest
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:unit
```

This runs the unit tests defined in `src/components/__tests__/MthElementForm.spec.ts`

### Deploy

```
.publish:
  stage: deploy
  before_script:
    - apt-get update && apt-get install -y git default-jre
    - npm install
    - npm run build
    - npm run chromatic
```

This builds the component as an npm package, and publish the story in chromatic, a website to view the storybook.

Use the component in another repository : 
```
import { MthElementForm } from "@metabohub/mth-element-form";  // import package
import "@metabohub/mth-element-form/dist/style.css";   // import style
```
